# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

INSTALL_REQUIREMENTS = [
    "requests>=2.6.0",
    "click>=7.0,<8.0",
    "xmltodict>=0.11,<0.13",
    "prettytable>=0.7,<0.8",
    "jsons>=1.0,<2.0",
]

setup(
    author="Jonas Ohrstrom",
    author_email="ohrstrom@gmail.com",
    url="https://gitlab.com/ohrstrom/icestat",
    name="icestat",
    version="0.0.1",
    description="Generates unified listener statistics in either JSON or console format",
    packages=find_packages(),
    install_requires=INSTALL_REQUIREMENTS,
    entry_points="""
        [console_scripts]
        icestat-cli=icestat:cli.icestat_cli
    """,
)
