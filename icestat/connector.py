# -*- coding: utf-8 -*-
import requests
import xmltodict
import time
import re
from dataclasses import dataclass
from requests.auth import HTTPBasicAuth
from requests.exceptions import ConnectionError, ReadTimeout, RequestException
from .exceptions import IcecastConnectorException


@dataclass
class Stats:
    mountpoints: list = ()
    listeners: list = ()

    def __post_init__(self):
        self.listeners.sort(key=lambda x: x.seconds_connected, reverse=True)

    def __repr__(self):
        return "Stats - num. listeners: {}".format(self.num_listeners)

    @property
    def num_listeners(self):
        return len(self.listeners)


@dataclass
class Listener:
    mountpoint: str
    ip: str
    user_agent: str
    seconds_connected: int = 0

    @property
    def time_connected(self):
        return time.strftime("%H:%M:%S", time.gmtime(self.seconds_connected))


class IcecastConnector(object):
    def __init__(self, url, username, password):
        self.url = url.rstrip("/")
        self.auth = HTTPBasicAuth(username, password)

    @staticmethod
    def _get(*args, **kwargs):
        try:
            r = requests.get(*args, **kwargs, timeout=(2, 10))
        except (ConnectionError, ReadTimeout, RequestException) as e:
            raise IcecastConnectorException(e)

        if not r.status_code == 200:
            raise IcecastConnectorException(
                "bad status code: {} - {}".format(r.status_code, r.text[0:100])
            )
            # raise IcecastConnectorException({"message":"My hovercraft is full of animals", "animal":"eels"})

        return r

    @staticmethod
    def _xml2dict(xml, **kwargs):
        return xmltodict.parse(xml, **kwargs)

    def _get_mountpoints(self):
        r = self._get("{}/admin/stats".format(self.url), auth=self.auth)
        response = self._xml2dict(r.text)

        for source in response.get("icestats", {}).get("source", []):
            yield source.get("@mount")

    def _get_listeners_by_mountpoint(self, mountpoint):
        params = {"mount": mountpoint}
        r = self._get(
            "{}/admin/listclients".format(self.url), params=params, auth=self.auth
        )
        response = self._xml2dict(r.text)

        for listener in (
            response.get("icestats", {}).get("source", {}).get("listener", [])
        ):
            try:
                yield Listener(
                    mountpoint,
                    listener.get("IP"),
                    listener.get("UserAgent"),
                    int(listener.get("Connected")),
                )
            except AttributeError as e:
                # print('---', e)
                continue

    def get_stats(self):
        mountpoints = list(self._get_mountpoints())
        listeners = []
        for mountpoint in mountpoints:
            listeners += list(self._get_listeners_by_mountpoint(mountpoint))
        return mountpoints, listeners


def filter_listeners_by_ignored_user_agents(listeners, ignored_user_agents):

    if not ignored_user_agents:
        return listeners

    patterns = [re.compile(p) for p in ignored_user_agents]
    filtered_listeners = []

    for listener in listeners:
        ua = listener.user_agent
        should_exclude = False
        for pattern in patterns:
            if pattern.search(ua):
                should_exclude = True
        if not should_exclude:
            filtered_listeners.append(listener)

    return filtered_listeners


def get_stats(server_url, username, password, ignored_user_agents):

    client = IcecastConnector(server_url, username, password)
    mountpoints, listeners = client.get_stats()

    listeners = filter_listeners_by_ignored_user_agents(listeners, ignored_user_agents)

    return Stats(mountpoints, listeners)
