# -*- coding: utf-8 -*-
from __future__ import absolute_import

import logging
import jsons
import sys
import click

from prettytable import PrettyTable
from icestat import __version__

from .connector import get_stats
from .exceptions import IcecastConnectorException

logger = logging.getLogger(__name__)


@click.group()
@click.option("-v", "--verbose", default=False, is_flag=True)
def icestat_cli(verbose):
    if verbose:
        logger.setLevel(logging.DEBUG)


@icestat_cli.command(name="version")
def cli_version():
    click.echo(__version__)
    sys.exit()


@icestat_cli.command(name="get-current-listeners", context_settings=dict(max_content_width=120))
@click.option(
    "--server",
    "-s",
    "server_url",
    envvar="ICECASTE_SERVER_URL",
    type=str,
    required=True,
    help="ICECASTE_SERVER_URL Icecast server URL.",
)
@click.option(
    "--user",
    "-u",
    "username",
    envvar="ICECASTE_USERNAME",
    type=str,
    required=True,
    help="ICECASTE_USERNAME Icecast admin user.",
)
@click.option(
    "--password",
    "-p",
    "password",
    envvar="ICECASTE_PASSWORD",
    type=str,
    required=True,
    help="ICECASTE_PASSWORD Icecast admin password.",
)
@click.option(
    "--ignore-ua",
    "-i",
    "ignored_user_agents",
    type=str,
    required=False,
    multiple=True,
    help="Ignore User-Agents by pattern. Multiple allowed",
)
@click.option(
    "--format",
    "-f",
    "output_format",
    type=click.Choice(["console", "json"], case_sensitive=False),
    default="json",
    required=False,
    show_default=True,
    help="Output format",
)
def cli_get_current_listeners(output_format, **kwargs):
    """
    Generates listener statistics from icecast2 server

    \b
    icestat-cli get-current-listeners \\
        --server http://stream.abc.org:8000/
        --user user-with-admin-rights
        --password ***

    \b
    # or using ENV variables
    ICECASTE_SERVER_URL=http://stream.abc.org:8000/ \\
    ICECASTE_USERNAME=*** \\
    ICECASTE_PASSWORD=*** \\
    icestat-cli get-current-listeners \\
        -f json \\
        -i "DABplayer" \\
        -i "Liquidsoap"

    """

    try:
        stats = get_stats(**kwargs)
    except IcecastConnectorException as e:
        click.secho("unable to get statistics: {}".format(e), fg="red", bold=True)
        sys.exit(1)

    if output_format == "console":

        tbl = PrettyTable(["IP", "Time connected", "Mountpoint", "User-Agent"])

        for i, f in enumerate(tbl.field_names):
            tbl.align[f] = "r" if i == 1 else "l"

        for listener in stats.listeners:
            tbl.add_row(
                (
                    listener.ip,
                    listener.time_connected,
                    listener.mountpoint,
                    listener.user_agent[0:60],
                )
            )

        click.echo(tbl)

    if output_format == "json":
        click.echo(jsons.dumps(stats, indent=4))
