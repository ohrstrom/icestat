# Configure Zabbix UserParameter


## Installation

    cd /opt/ && git clone https://gitlab.com/ohrstrom/icestat.git && cd icestat
    virtualenv -p python3.7 ./venv
    source ./venv/bin/activate
    pip install -e .

## Zabbix script

    ln -s /opt/icestat/zabbix/icestat.conf /etc/zabbix/zabbix_agentd.conf.d/icestat.conf

## Configuration

    nano /opt/icestat/.env

    > export ICECASTE_SERVER_URL=https://stream-server.org/s
    > export ICECASTE_USERNAME=***
    > export ICECASTE_PASSWORD=***
