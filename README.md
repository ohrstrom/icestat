# Icecast2 Listener Statistics


Generates listener statistics from icecast2 in JSON format.

Starting with version 2.4 icecast provides 
[JSON formatted information](http://www.icecast.org/docs/icecast-trunk/server_stats/) (`status-json.xsl`) - but 
not in a detailed form.

We need statistics aggregated over all mountpoints, and connection details containing client address (`ip`) 
and the time connected (`seconds_connected`).

`icestat-cli` reads state from the `admin` endpoint (XML) and converts it to a summarized format.


#### Example Output

##### JSON
    
```json
{
  "num_listeners": 439,
  "mountpoints": [
    "/foo.flac",
    ...
  ],
  "listeners": [
    {
      "ip": "178.***.***.***",
      "mountpoint": "/foo.flac",
      "seconds_connected": 14016,
      "time_connected": "03:53:36",
      "user_agent": "iTunes/4.7.1 (Unix; N; linux; x86_64-linux; EN; utf8) SqueezeNetwork/7.7.7-sn/TRUNK"
    },
    ...
  ],
}
```

##### Console

```
+-----------------+----------------+------------------------+--------------------------------------------------------------+
| IP              | Time connected | Mountpoint             | User-Agent                                                   |
+-----------------+----------------+------------------------+--------------------------------------------------------------+
| 178.***.***.*** |       03:53:36 | /foo.flac              | iTunes/4.7.1 (Unix; N; linux; x86_64-linux; EN; utf8) Squeez |
| ...             |            ... | ...                    | ...                                                          |
+-----------------+----------------+------------------------+--------------------------------------------------------------+
```



## Install

    git clone https://gitlab.com/ohrstrom/icestat.git
    cd icestat
    pip install -e .
    
## Usage

    icestat-cli get-current-listeners --help
    

## Zabbix integration

See [Configure Zabbix UserParameter](./zabbix/README.md).
